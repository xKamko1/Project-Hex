using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Highlight_element : MonoBehaviour
{
    private MeshRenderer _renderer;
    private Color _colorGlow;
    private Color _colorOld;

    private float _glowPower = 0.15f;

    public bool keepOutline = false;

    private void Start()
    {
        _renderer = this.gameObject.GetComponent<MeshRenderer>();

        _colorOld = _renderer.material.color;
        _colorGlow = new Color(_glowPower, _glowPower, _glowPower);

        gameObject.AddComponent<Outline>();
    }

    private void OnMouseEnter()
    {
        if(!(this.gameObject.GetComponent<Outline>()) && !keepOutline)
        {
            //adding outline to the object
            var outline = gameObject.AddComponent<Outline>();
            outline.OutlineMode = Outline.Mode.OutlineAll;
            outline.OutlineColor = Color.yellow;
            outline.OutlineWidth = 5f;

            //adding glow to the object
            _renderer.material.color += _colorGlow;
        }
        


        
    }

    private void OnMouseExit()
    {
        if(!keepOutline)
        {
            Disable_Outline();
        }
    }

    public void Disable_Outline()
    {
        if (this.gameObject.GetComponent<Outline>())
        {
            Destroy(this.gameObject.GetComponent<Outline>());
            _renderer.material.color = _colorOld;
        }
    }
}
