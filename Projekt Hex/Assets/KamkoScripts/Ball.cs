using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fusion;

public class Ball : NetworkBehaviour
{
    [Networked] private TickTimer life { get; set; }

    //direction in which we shoot
    private Vector3 dir;
    //Target object
    private GameObject _Target;

    private float shot_speed = 15f;

    public void Init() 
    {
        life = TickTimer.CreateFromSeconds(Runner, 5.0f);

        if (SearchForEnemy.EnemyObject != null)
        {
            _Target = SearchForEnemy.EnemyObject;
        }
        else
        {
            _Target = null;
            
        }

        if(_Target)
        {
            //Calculate the road between spawn and the target;
            dir = _Target.transform.position - transform.position;
            dir = dir.normalized;
        }
        
    }

    public override void FixedUpdateNetwork()
    {
        

        if (life.Expired(Runner))
        {
            Runner.Despawn(Object);
        }
        else
        {
            //transform.position += shot_speed * transform.forward * Runner.DeltaTime;
            MoveShot();
        }
    }

    void MoveShot()
    {
        if (this.gameObject != null)
        {
            //this.gameObject.GetComponent<Rigidbody>().velocity = dir * shot_speed;
            if(_Target)
                transform.position += shot_speed * dir * Runner.DeltaTime;
            else
            {
                transform.position += shot_speed * transform.forward * Runner.DeltaTime;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("kolizja");
        Runner.Despawn(Object);
    }
}
