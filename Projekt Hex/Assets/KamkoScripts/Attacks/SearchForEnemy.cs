using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchForEnemy : MonoBehaviour
{
    public static GameObject EnemyObject;

    private bool enemy_found = false;

    private enum Scale { small, medium, big, max }
    // (4,2,4) ; (8,4,8) ; (14,7,14) ; (20,10,20)

    Scale my_scale;
    void Start()
    {
        EnemyObject = null;

        my_scale = Scale.small;

       // this.gameObject.transform.localScale = new Vector3(4, 2, 4);
    }

    void Update()
    {
        ScaleSphere();
    }

    void ScaleSphere()
    {
        if (enemy_found == false)
        {
            if (my_scale == Scale.small)
            {
                my_scale = Scale.medium;
                this.gameObject.transform.localScale = new Vector3(8, 4, 8);
            }
            else if (my_scale == Scale.medium)
            {
                my_scale = Scale.big;
                this.gameObject.transform.localScale = new Vector3(14, 7, 14);
            }
            else if (my_scale == Scale.big)
            {
                my_scale = Scale.max;
                this.gameObject.transform.localScale = new Vector3(20, 10, 20);
            }
            else
            {
                my_scale = Scale.small;
                this.gameObject.transform.localScale = new Vector3(4, 2, 4);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //if(other.gameObject.tag == "Decoy")
        if (other.gameObject.GetComponent<Player>())
        {
            enemy_found = true;
            EnemyObject = other.gameObject;
            //Debug.Log("Enemy detected");

            Debug.Log(EnemyObject);
        }
        else
        {
            EnemyObject = null;
            enemy_found = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        //if(other.gameObject.tag == "Decoy")
        if (other.gameObject.GetComponent<Player>())
        {
            EnemyObject = other.gameObject;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Decoy")
        //if (other.gameObject.GetComponent<Player>())
        {
            EnemyObject = other.gameObject;
        }
    }
}
